<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>test</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,900" rel="stylesheet" type="text/css">
    <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  </head>
  <body>
    <!-- container-->
    <div id="jamon" class="container">
      <div class="col-xs-12">
        <div class="profile-image"><img src="assets/img/avatar.jpg" alt="profile-image" class="img-responsive"></div>
      </div>
      <div class="col-xs-12 text-center">
        <h1>Roger Sterling</h1>
      </div>
      <div class="col-xs-12 text-center">
        <h3>Accountant & fron-end Developer</h3>
      </div>
      <div class="col-xs-12 text-center">
        <div class="social-icon"><span class="lsf">mail</span><span class="lsf">twitter</span><span class="lsf">linkedin</span><span class="lsf">web</span></div>
      </div>
      <div class="col-xs-12 text-center">
        <span>Manhattan, NY</span>
      </div>
      <br>
      <div class="col-xs-12 text-center">
        <div class="skill col-xs-6">html5</div>
        <div class="skill col-xs-6">less</div>
        <div class="skill col-xs-6">javascript</div>
        <div class="skill col-xs-6">design patterns</div>
      </div>
    </div>
    <!-- footer-->
    <nav class="navbar navbar-fixed-bottom">
      <div class="mobile-bottom-menu text-center"><a href="#"> <span class="option lsf">book</span></a><a href="#"><span class="option lsf">folder</span></a><a href="#"><span class="option lsf">setting</span></a></div>
    </nav>
  </body>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
  <script src="js/main.js"></script>
</html>