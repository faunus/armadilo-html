var inicio = (function ($) {
	// camelCase for module-local vars
	var variables = {},
	jquery ={},
	currentHeight = $(window).innerHeight(),
	offset, initModule, containerHeight, addMarginTop;
 
	// dom methods
	addMarginTop = function (offset) {
		jquery.currentElement
			.css("margin-top", offset);
	};
 
	// event handlers
 
	// public method
 
	centerHorizontal = function (element) {
		jquery.currentElement = element;
		// console.log(currentHeight - element.height());
		offset = (currentHeight - element.height()) / 2;
		if (offset <= 1){
			addMarginTop(18);
		} else {
			addMarginTop(offset);
		}
		// console.log(offset);
		return true;
	};
 
	return { center : centerHorizontal };
}($));
 
 inicio.center($('#jamon'));
